Author: **Roman Mattia**

RedID: 820771491

GitLab: http://rijeka.sdsu.edu/Roman_Mattia/cs583_f19_2d_mattia_roman_thread

**T H R E A D - Story & Instructions:**

Set in the futuristic dystopian landscape of *NOR Cost*, our unlikely protagonist - ***Howard*** - is racing against the clock to escape the city limits.

Who - *or what* - is following him?

Why must he escape the city limits?

Only you can solve that mystery and lead ***Howard*** to safety. Navigate him through the treacherous landscape of *NOR Cost* using your **Holo-Communicator** (Space Bar).

The jumps in *NOR Cost* can be deceiving. Luckily, ***Howard*** is equip with futuristic neural synapse enhancements & the latest in biomechatronic body modifications. Repeatedly pressing your **Holo-Communicator** (Space Bar) will signal ***Howard*** to climb vertical surfaces. Over time, ***Howard*** will begin to see the world in a blur. As his journey progresses his cyborg enhancements will allow him to run faster and jump higher.

Will you be able to keep up and lead him to safety?

I guess we'll find out- ***Good Luck***!

~***R***

**Works Cited:**

    https://sanderfrenken.github.io/Universal-LPC-Spritesheet-Character-Generator/
    
    https://www.youtube.com/channel/UCyBsvsU7uiurMiBZIYXvnyg

    https://www.youtube.com/watch?v=ou8VkQB2sos

    https://assetstore.unity.com/packages/2d/environments/warped-city-assets-pack-138128
    
    https://learn.unity.com/tutorial/unity-for-2d-new-workflows-in-unity-4-3#
    
    https://www.youtube.com/channel/UC9Z1XWw1kmnvOOFsj6Bzy2g
    
    https://www.youtube.com/watch?v=epRPKFsOPck
    
    https://learn.unity.com/project/beginner-gameplay-scripting
    
    https://assetstore.unity.com/packages/audio/music/electronic/dark-future-music-3777
    
    https://www.youtube.com/watch?v=KOf3P5y19Bw