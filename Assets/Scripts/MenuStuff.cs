﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuStuff : MonoBehaviour
{
    public void Start(int SceneIndex)
    {
        SceneManager.LoadScene(SceneIndex);
    }

    public void End()
    {
        Application.Quit();
        //Debug.Log("Application Quit");
    }
}
