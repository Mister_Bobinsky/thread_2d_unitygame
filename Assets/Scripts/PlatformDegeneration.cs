﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformDegeneration : MonoBehaviour
{
    public GameObject platformDegenerationPoint;
    
    // Start is called before the first frame update
    void Start()
    {
        platformDegenerationPoint = GameObject.Find("PlatformDegenerationPoint");
    }

    // Update is called once per frame
    void Update()
    {
        if(transform.position.x < platformDegenerationPoint.transform.position.x)
        {
            //V.2
            gameObject.SetActive(false);
        }
    }
}
