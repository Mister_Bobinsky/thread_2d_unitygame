﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DeathStuff : MonoBehaviour
{
    public void backToStart(int SceneIndex)
    {
        SceneManager.LoadScene(SceneIndex);
    }

    public void resetAll()
    {
        FindObjectOfType<GameMechanics>().Reset();
    }
}
