﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMechanics : MonoBehaviour
{
    public Transform platformformGenerator;
    private Vector3 platformSetPoint;
    public PlayerMovementController mainCharacter;
    private Vector3 characterSetPoint;
    private PlatformDegeneration[] platformTracker;
    private ScoreTracker scoreTracker;
    public DeathStuff death;
    
    // Start is called before the first frame update
    void Start()
    {
        platformSetPoint = platformformGenerator.position;
        characterSetPoint = mainCharacter.transform.position;
        scoreTracker = FindObjectOfType<ScoreTracker>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ResetGame()
    {
        scoreTracker.isPoints = false;
        mainCharacter.gameObject.SetActive(false);
        death.gameObject.SetActive(true);
    }

    public void Reset()
    {
        death.gameObject.SetActive(false);
        platformTracker = FindObjectsOfType<PlatformDegeneration>();

        for (int i = 0; i < platformTracker.Length; i++)
        {
            platformTracker[i].gameObject.SetActive(false);
        }

        mainCharacter.transform.position = characterSetPoint;
        platformformGenerator.position = platformSetPoint;
        mainCharacter.gameObject.SetActive(true);
        scoreTracker.playerScoreCtr = 0;
        scoreTracker.isPoints = true;
    }
}
