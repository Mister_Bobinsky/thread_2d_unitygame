﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementController : MonoBehaviour
{
    public float moveSpeed;
    public float jumpForce;

    private Rigidbody2D mainCharacterRigidBody;

    public bool isGrounded;
    public LayerMask setGround;

    private Collider2D mainCharacterCollider;

    private Animator mainCharacterAnimator;

    public GameMechanics gameMechanics;

    public float moveSpeedMult;
    public float moveSpeedIncMarker;
    private float moveSpeedMarkerCtr;

    private float moveSpeedTrack;
    private float moveSpeedCtrTrack;
    private float moveSpeedIncTrack;
    
    // Start is called before the first frame update
    void Start()
    {
        mainCharacterRigidBody = GetComponent<Rigidbody2D>();

        mainCharacterCollider = GetComponent<Collider2D>();

        mainCharacterAnimator = GetComponent<Animator>();

        moveSpeedMarkerCtr = moveSpeedIncMarker;

        moveSpeedTrack = moveSpeed;

        moveSpeedCtrTrack = moveSpeedMarkerCtr;

        moveSpeedIncTrack = moveSpeedIncMarker;
    }

    // Update is called once per frame
    void Update()
    {
        isGrounded = Physics2D.IsTouchingLayers(mainCharacterCollider, setGround);

        if(transform.position.x > moveSpeedMarkerCtr)
        {
            moveSpeedMarkerCtr += moveSpeedIncMarker;
            moveSpeedIncMarker *= moveSpeedMult;
            moveSpeed *= moveSpeedMult;
        }

        mainCharacterRigidBody.velocity = new Vector2(moveSpeed, mainCharacterRigidBody.velocity.y);

        if(Input.GetKeyDown(KeyCode.Space))
        {

            if (isGrounded)
            {
                mainCharacterRigidBody.velocity = new Vector2(mainCharacterRigidBody.velocity.x, jumpForce);
            }
        }

        mainCharacterAnimator.SetFloat("Speed", mainCharacterRigidBody.velocity.x);
        mainCharacterAnimator.SetBool("isGrounded", isGrounded);
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "reset")
        {
            gameMechanics.ResetGame();
            moveSpeed = moveSpeedTrack;
            moveSpeedMarkerCtr = moveSpeedCtrTrack;
            moveSpeedIncMarker = moveSpeedIncTrack;
        }
    }
}
