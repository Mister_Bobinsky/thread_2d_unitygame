﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformGenerator : MonoBehaviour
{
    public GameObject generatedPlatform;
    public Transform generationPoint;
    public float distanceBetweenPlatforms;

    private float platformWidth;

    public float distBetweenPlatMin;
    public float distBetweenPlatMax;

    private int platformSelection;
    private float[] platformWidths;

    public ObjectPool[] theObjectPool;

    private float minHeight;
    private float maxHeight;
    public Transform maxHeightSet;
    public float maxHeightChange;
    private float heightChange;
    
    // Start is called before the first frame update
    void Start()
    {
        platformWidths = new float[theObjectPool.Length];
        for(int i = 0; i < theObjectPool.Length; i++)
        {
            platformWidths[i] = theObjectPool[i].pooledObject.GetComponent<BoxCollider2D>().size.x;
        }

        minHeight = transform.position.y;
        maxHeight = maxHeightSet.position.y;
    }

    // Update is called once per frame
    void Update()
    {
        if(transform.position.x < generationPoint.position.x)
        {
            distanceBetweenPlatforms = Random.Range(distBetweenPlatMin, distBetweenPlatMax);

            platformSelection = Random.Range(0, theObjectPool.Length);

            heightChange = transform.position.y + Random.Range(maxHeightChange, -maxHeightChange);

            if(heightChange > maxHeight)
            {
                heightChange = maxHeight;
            } else if(heightChange < minHeight)
            {
                heightChange = minHeight;
            }

            transform.position = new Vector3(transform.position.x + platformWidths[platformSelection] + distanceBetweenPlatforms, heightChange, transform.position.z);

            //V.2
            GameObject newPlatform = theObjectPool[platformSelection].GetPooledObject();
            newPlatform.transform.position = transform.position;
            newPlatform.transform.rotation = transform.rotation;
            newPlatform.SetActive(true);
        }
    }
}
