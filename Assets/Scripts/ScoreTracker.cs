﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreTracker : MonoBehaviour
{
    public Text playerScore;
    public Text highScore;

    public float playerScoreCtr;
    public float highScoreCtr;
    public float pointsCtr;

    public bool isPoints;
    
    // Start is called before the first frame update
    void Start()
    {
        if(PlayerPrefs.HasKey("High Score"))
        {
            highScoreCtr = PlayerPrefs.GetFloat("High Score");
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(isPoints)
        {
            playerScoreCtr += pointsCtr * Time.deltaTime;
        }

        if(playerScoreCtr > highScoreCtr)
        {
            highScoreCtr = playerScoreCtr;
            PlayerPrefs.SetFloat("High Score", highScoreCtr);
        }

        playerScore.text = "Player Score: " + Mathf.Round(playerScoreCtr);
        highScore.text = "High Score: " + Mathf.Round(highScoreCtr);
    }
}
